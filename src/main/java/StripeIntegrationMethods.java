import com.stripe.exception.StripeException;
import com.stripe.model.Charge;
import com.stripe.model.Customer;

import java.util.List;

public interface StripeIntegrationMethods {

    Customer getUserTokenByEmail(String Email) throws StripeException;
    Charge cobrarValor(Customer customer, Integer valor, String moeda) throws StripeException;
    boolean reembolsar(Charge charge) throws StripeException;
    List<Customer> obterListaCustomers() throws StripeException;
    boolean testarCustomerPayment(Customer customer) throws StripeException;
    Customer criarCustomer(String email, String name);
    boolean associarCartaoCustomer(Customer customer, String cardNumber, String expMonth, String expYear, String CVC);
}
