import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.model.Charge;
import com.stripe.model.Customer;
import com.stripe.model.CustomerCollection;
import com.stripe.model.Refund;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StripeIntegration implements StripeIntegrationMethods{

    public StripeIntegration(String api_key) {
        Stripe.apiKey = api_key;
    }

    @Override
    public Customer getUserTokenByEmail(String email) throws StripeException {
        if (email == null) return null;
        Map<String, Object> parametros = new HashMap<>();
        parametros.put("email", email);
        List<Customer> cc = Customer.list(parametros).getData();
        return cc.size() == 1 ? cc.get(0) : null;
    }

    @Override
    public Charge cobrarValor(Customer customer, Integer valor, String moeda) throws StripeException {
        if (customer == null || valor == null || moeda == null) {
            return null;
        }

        Integer finalValue = valor * 100;

        Map<String, Object> chargeParams = new HashMap<>();

        chargeParams.put("amount", finalValue.toString());
        chargeParams.put("currency", moeda);
        chargeParams.put("customer", customer.getId());

        Charge charge = Charge.create(chargeParams);
        return charge.getPaid() ? charge : null;
    }

    @Override
    public boolean reembolsar(Charge charge) throws StripeException {
        if (charge == null || charge.getRefunded()) {
            return false;
        }

        Map<String, Object> parametros = new HashMap<>();
        parametros.put("charge", charge.getId());

        Refund.create(parametros);

        return charge.getRefunded();
    }

    @Override
    public List<Customer> obterListaCustomers() throws StripeException {
        return Customer.list(new HashMap<>()).getData();
    }

    @Override
    public boolean testarCustomerPayment(Customer customer) throws StripeException {
        if (customer == null) return false;
        Charge c = cobrarValor(customer, 1, "usd");
        if (!c.getPaid()) return false;
        reembolsar(c);
        return true;
    }

    @Override
    public Customer criarCustomer(String email, String name) {
        throw new NotImplementedException();
    }

    @Override
    public boolean associarCartaoCustomer(Customer customer, String cardNumber, String expMonth, String expYear, String CVC) {
        throw new NotImplementedException();
    }
}
